#!/usr/bin/python3
# coding: utf-8

from flask import Flask, request, Response, send_file
import json
from bson import json_util
from bson.objectid import ObjectId
from werkzeug.utils import secure_filename
from werkzeug.datastructures import ImmutableMultiDict
import os

from pix import app, mongo, UPLOADS_DIR, THUMBNAILS_DIR, ALLOWED_EXTENSIONS

def to_json(data):
    return json.dumps(data, default=json_util.default)

@app.route('/api/pictures/latest_pictures')
def home():
    json_results = {}
    pictures_cursor = mongo.db.pictures.find({}).limit(4)
    pictures = []

    for picture in pictures_cursor:
        pictures.append(picture)

    json_results.update({'pictures': pictures})
    return Response(to_json(json_results), status=200, mimetype='application/json')

@app.route('/api/pictures/get/<picture_slug>')
def get(picture_slug):
    json_results = {}
    picture = mongo.db.pictures.find_one({"slugify": picture_slug})
    json_results.update({'picture': picture})
    return to_json(json_results)

@app.route('/api/pictures/thumbnail/<file_name>')
def get_thumbnail(file_name):
    return send_file(THUMBNAILS_DIR + '/' + file_name, mimetype='image/jpg')

@app.route('/api/pictures/up', methods=['POST'])
def upload_picture():
    if request.method == 'POST':
        if 'file' not in request.files:
            return Response(to_json({'error': 'File doesn\'t exist'}), status=400)
        file = request.files['file']
        if file.filename == '':
            return Response(to_json({'error': 'empty filename'}), status=400)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOADS_DIR, filename))
            return Response(to_json({'status': 'ok'}), status=200)
    