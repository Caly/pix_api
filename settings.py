#!/usr/bin/python3
# coding: utf-8

import os

class Config(object):
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    DEBUG = False

    # Mongo config
    MONGO3_HOST = 'localhost'
    MONGO3_PORT = 27017
    MONGO3_DBNAME = 'pix'
    SECRET_KEY = "still in dev"

class DevelopmentConfig(Config):
    DEBUG = True
