#!/usr/bin/python3
# coding: utf-8

from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS, cross_origin
import os

app = Flask(__name__)
app.config.update(
    BASE_DIR = os.path.abspath(os.path.dirname(__file__)),
    UPLOADS_DIR = os.path.abspath(os.path.dirname(__file__)) + '/media/pictures',
    THUMBNAILS_DIR = os.path.abspath(os.path.dirname(__file__)) + '/media/thumbnails',
    DEBUG = False,
    MONGO3_HOST = 'localhost',
    MONGO3_PORT = 27017,
    MONGO3_DBNAME = 'pix',
    SECRET_KEY = 'still in dev'
)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
mongo = PyMongo(app, config_prefix='MONGO3')

UPLOADS_DIR = app.config['UPLOADS_DIR']
THUMBNAILS_DIR = app.config['THUMBNAILS_DIR']
ALLOWED_EXTENSIONS = set(['jpg', 'png', 'gif', 'apng'])

import pix.views
